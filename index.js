/** CRUD Operations
 * - CRUD operations are the heart of any backend application.
 * - mastering the CRUD operations is essential for any developer.
 * - This helps in building character and increasing exposure to logical statements that will help us manipulate.
 * - mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with hure amounts of information.
 */

// [Sections] Inserting document (Create)
/**
 * Insert one document
 *  - since MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
 *  - mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code.
 * Syntax:
 *  - db.collectionName.insertOne({objectName});
 * JavaScript:
 *    object.object.method({object})
 */
db.users.insertOne({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
    phone: "12345678910",
    email: "roda@gmail.com",
  },
  courses: ["CSS", "JavaScript", "Python"],
  department: "none",
});
/**
 * Insert Many
 *  - Syntax:
 *    db.collectionName.insertMany([{objectA}, {objectB}])
 */

db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87412421",
      email: "hawking@gmail.com",
    },
    courses: ["React", "PHP", "Python"],
    department: "none",
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "12312",
      email: "armstrong@gmail.com",
    },
    courses: ["React", "Laravel", "SASS"],
    department: "none",
  },
]);
/**
 * [Section] Finding Documents (read)
 * Find
 *  - if multiple documents match the criteria for finding a document only the first document that matches the search term will be returned
 * - this is based from the order that the documents stored in a collection
 * Syntax:
 *    db.collectionName.find();
 *    db.collectionName.find({filed: value})
 */
// leaving the search criteria empty will retrieve all the documents
db.users.find();
db.users.find({ firstName: "Stephen" });
/**
 * pretty method
 * - The pretty method allows us to be able to view the documents returned by our terminals in a better format.
 */
db.users.find({ firstName: "Stephen" }).pretty();
/**
 * finding documents with multiple parameters
 * Syntax:
 *    db.collectionName.find({fieldA: valueA, valueB: valueB})
 */
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();

// [Section] Updating documents

// Updatiding a single document
db.users.insert({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "0000000",
    email: "test@gmail.com",
  },
  courses: ["React", "Laravel", "SASS"],
  department: "none",
});

/**
 * - just like the fluid method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated.
 *
 * Syntax:
 *    db.collectionName.updateOne({criteria},{&set: {field:value}})
 */

db.users.updateOne(
  { firstName: "Neil" },
  {
    $set: {
      contact: {
        phone: "87412421",
      },
    },
  }
);
db.users.updateOne(
  { firstName: "Bill" },
  {
    $set: {
      lastName: "Gates",
      age: 65,
      contact: {
        phone: "12345678910",
        email: "gates@gmail.com",
      },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations",
      status: "active",
    },
  }
);
// Updating multiple documents
/**
 *    db.collectionName.updateMany({criteria}, {$set: {field: value}})
 */
db.users.updateMany(
  { department: "none" },
  {
    $set: {
      department: "HR",
    },
  }
);

// Replace One
// can be used if replacing the whole document necessary
/**
 * Syntax:
 *      db.collectionName.replaceOne({criteria}, {$set: { object }})
 */
db.users.replaceOne(
  { _id: ObjectId("637f101690261d40aeedb27f") },
  {
    $set: {
      firstName: "Jayson",
      lastName: "Roda",
      age: 14,
      contact: {
        phone: "12345678",
        email: "roda@gmail.com",
      },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations",
    },
  }
);

// [Section] Deleting documents (delet)
db.users.insert({
  firstName: "test",
});

// Deleting a single document
/**
 * Syntax:
 *    db.collectionName.deleteOne({criteria});
 */
// the first document in collection will be deleted
db.users.deletOne({});
// find the first one who has firstName
db.users.deleteOne({ firstName: "test" });

// delete many
/**
 * - be careful when using the deleteMany method. if no search criteria is provided it will delete all documents in the collections
 * - DO NOT USE: database
 */

db.users.deleteMany({ firstName: "Jayson" });

// [Section] Advanced queries
// Query an embedded document
db.users.find({
  contact: {
    phone: "87412421",
    email: "hawking@gmail.com",
  },
});

// Query on nested field
db.users.find({ "contact.phone": "87412421" });

// Query an array with exact elements
db.users.find({ courses: ["React", "PHP", "Python"] });

// Queryin an
db.users.find({ courses: { $all: ["React"] } });
